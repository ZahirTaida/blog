from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.urls import reverse
from .models import Post
# Create your tests here.


class BlogTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='test',
            email='test',
            password='test'
        )
        self.post = Post.objects.create(
            title='A good title',
            body='dump body',
            author=self.user
        )
    
    def test_string_representation(self):
        post = Post(title='A sample title')
        self.assertEqual(str(post), post.title)
    
    def test_post_content(self):
        self.assertEqual(str(self.post.title), 'A good title')
        self.assertEqual(str(self.post.author), 'test')
        self.assertEqual(str(self.post.body), 'dump body')
    
    def test_post_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'dump body')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/1212/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'test')
        self.assertTemplateUsed(response, 'post_detail.html')

    def test_post_create_view(self):
        response = self.client.post(
            reverse('new_post'), {
                'title': 'testTitle',
                'body': 'TestBody',
                'author': self.user
                }
            )
        
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'testTitle')

    
    def test_post_edit_view(self):
        response = self.client.get(
            reverse('edit_post', args='1'),
        {'title': 'editedTitleForTest', 'body': 'editedBodyForTest'}
        )
        self.assertEqual(response.status_code, 200)
    
    def test_post_delete_view(self):
        response = self.client.get(
            reverse('delete_post', args='1')
        )
        self.assertEqual(response.status_code, 200)